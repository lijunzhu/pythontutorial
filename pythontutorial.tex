\documentclass[compress]{beamer}

\mode<presentation> {
\usetheme{Luebeck}
\hypersetup{pdfpagemode=FullScreen}
\setbeamertemplate{footline}[frame number] % To replace the footer line in all slides with a simple slide count uncomment this line
\setbeamertemplate{headline}[default]
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{caption}[numbered]

% define your own colors:
\definecolor{gold}{rgb}{260,195,0}
%\definecolor{GTgold}{cmyk}{0,0.241,0.933,0.122}
\definecolor{KFUPMgreen}{cmyk}{0.756,0,0.756,0.196}

% Make GT-CSIP footer logo scheme
\setbeamercolor{structure}{fg = KFUPMgreen,bg=blue!40!gold}
\setbeamercolor*{palette primary}{use=structure,fg=white,bg=blue!40!gold}
}

% Packages
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{mathtools}
\usepackage{graphicx}
\graphicspath{{fig/}}
\usepackage{hyperref}
\hypersetup{colorlinks=true}
\usepackage{listings}
\usepackage{minted}

% Title
\title{Python Tutorial for ECE 6254}
\author[shortname]{Lijun Zhu, Liangbei Xu}
\institute{Georgia Tech}
\date{\today}

%  Outline at each section
\AtBeginSection[] {
  \begin{frame}
   \frametitle{Outline}
    \tableofcontents[currentsection]
    \addtocounter{framenumber}{-1}
  \end{frame}
}

\begin{document}
\begin{frame}[noframenumbering,plain]
\titlepage
\end{frame}

\setcounter{framenumber}{0}

\begin{frame}
\frametitle{Outline}
\tableofcontents
\end{frame}
    
\section{Introduction}

\label{sec:intro}
% Objectives:
%   1. What is Python
%   2. Why choose Python over other programs (MATLAB, R)
%   3. What are the skills required for Python

\begin{frame}
    \frametitle{Objectives}
    \begin{itemize}
        \item What is Python?
        \vspace{1cm}
        \item How does Python compare to other languages?
        \vspace{1cm}
        \item What are the basic skills/tools needed to do scientific computing in Python?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{A short list of facts about Python}
    \begin{itemize}
        \item A well-adopted interpreted language for scientific computing
        \vspace{1cm}
        \item ··Python is written in English‘’ (\href{https://docs.python.org/3.4/reference/}{The Python Language Reference})
        \vspace{1cm}
        \item Diversified toolbox/libraries for testing, especially machine learning algorithms
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Popular among company and community}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{tiobe}
        \caption{TIOBE Index (Jan 2017)}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Popular among company and community}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{redmonk}
        \caption{RedMonk Rank (Jun 2016)}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A typical Python program}
    % \lstinputlisting[language=Python]{code/example.py}
    \inputminted{python}{code/example.py}
\end{frame}

\begin{frame}
    \frametitle{Useful Python toolbox}
    \begin{itemize}
        \item Signal processing: Numpy, Scipy, Matplotlib
        \vspace{1cm}
        \item Image processing and classical ML: scikit-image, scikit-learn
        \vspace{1cm}
        \item Deap learning: TensorFlow, Torch, Theano
    \end{itemize}
\end{frame}

\section{Setup Environment}
% Objectives:
%   1. Anaconda environment setup
%   2. Virtual/Conda environment explanation
%   3. Package management brief
%   4. IDE recommendation
%       a) PyCharm + iPython
%       b) Spyder
%       c) other editor + iPython

\begin{frame}
    \frametitle{Anaconda: setup Python environment}
    \begin{itemize}
        \item An integrated environment for scientific computing
        \begin{itemize}
            \item Python interpreter (2 or 3) implemented in C
            \item Most common packages for computing
            \item Well tested software
        \end{itemize}
        \vspace{1cm}
        \item One click installation
        \begin{itemize}
            \item Easy installation for three major OS's
            \item Unified experience
        \end{itemize}
        \vspace{1cm}
        \item Fully functioning package manager
        \begin{itemize}
            \item conda: a "apt" like CLI
            \item navigator: a GUI for package management
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Download Anaconda online}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\linewidth]{anaconda_download}
        \caption{Anaconda \href{https://www.continuum.io/downloads}{download} page.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Navigator}
    \begin{figure}
        \centering
        \includegraphics[width=0.95\linewidth]{navigator}
        \caption{Anaconda navigator program}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Conda}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{conda}
        \caption{Screen capture of results for "conda --help."}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Spyder: MATLAB-like IDE}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{spyder}
        \caption{Spyder user interface.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{PyCharm: Professional IDE}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{pycharm}
        \caption{PyCharm user interface.}
    \end{figure}
\end{frame}

\section{Basic Techniques}
% Objectives:
%   1. Brief intro to Python data structure and control flow
%   2. Numpy array intro
%   3. Common usage of Numpy array

\begin{frame}
    \frametitle{Basic Python data structure}
    \begin{itemize}
        \item List: mutable array for (ordered) homogeneous data
        \begin{itemize}
            \item array = [0,1,2,3,4]
            \item a,b,c,d,e = array
        \end{itemize}
        \vspace{0.5cm}
        \item Tuple: immutable array for (structured) heterogeneous data
        \begin{itemize}
            \item time = (2017, 'Jan', 23, 12, 0, 'pm') 
        \end{itemize}
        \vspace{0.5cm}
        \item Dictionary: hash table
        \begin{itemize}
            \item dic = \{'a': 1, 'b': 2\}
        \end{itemize}
        \vspace{0.5cm}
        \item Set: hash table with dummy keys 
        \begin{itemize}
            \item s = \{1, 2, 3\}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Control flow tools}
    \begin{itemize}
        \item if statement
        \vspace{0.5cm}
        \item for statement
        \vspace{0.5cm}
        \item range() function (iterable)
        \vspace{0.5cm}
        \item break and continue statement
        \vspace{0.5cm}
        \item pass statement
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{If statement}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{if}
        \caption{If statement example}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{For statement}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{for}
        \caption{For statement example}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Range class}
    \begin{itemize}
        \item range is a class of immutable iterable objects
        \item iterable is essential to make for loop and list comprehension work
    \end{itemize}
    \begin{figure}
        \begin{minipage}{0.4\linewidth}
            \centering
            \includegraphics[width=\linewidth]{range1}
            \caption{Range usage.}
        \end{minipage}
        \begin{minipage}{0.58\linewidth}
            \centering
            \includegraphics[width=\linewidth]{range2}
            \caption{Range in for loop.}
        \end{minipage}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Iterable, Iterator, and Generator}
    Try to distinguish the following concepts:

    \begin{minipage}{0.38\linewidth}
        \begin{itemize}
        \item a container
        \item an iterable
        \item an iterator
    \end{itemize}
    \end{minipage}
    \begin{minipage}{0.58\linewidth}
            \begin{itemize}
        \item a generator
        \item a generator expression
        \item a {list, set, dict} comprehension
    \end{itemize}
    \end{minipage}

    \pause
    
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{relationships}
        \caption{Relationship between Python concepts (\href{http://nvie.com/posts/iterators-vs-generators/}{reference}).}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Break statement}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{break}
        \caption{Break statement example.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Continue statement}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{continue}
        \caption{Continue statement example}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Pass statement}
    \begin{itemize}
        \item Do nothing
        \includegraphics[width=\linewidth]{pass1}
        \item Create minimal class
        \includegraphics[width=\linewidth]{pass2}
        \item Place-holder
        \includegraphics[width=\linewidth]{pass3}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Defining functions}
    \begin{itemize}
        \item Functions are also objects in Python
        \item Created by "def" statement
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{function}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lambda expression}
    \begin{itemize}
        \item Keyword lambda defines a anonymous function
        \item They can be used as input and output of a normal function
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{lambda1}
        \caption{Lambda expression as output}
    \end{figure}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{lambda2}
        \caption{Lambda expression as input}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Documentation string}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{dcostring}
        \caption{Documentation string example.}
    \end{figure}
\end{frame}

\section{Numpy array}

\begin{frame}
    \frametitle{Relationship to Matlab}
    \begin{itemize}
        \item Python's core scientific computing modules are Numpy, Scipy, and Matplotlib
        \begin{figure}
            \centering
            \includegraphics[width=\linewidth]{to-matlab}
            \caption{Three packages make up the core Matlab function (\href{http://www.python-course.eu/numpy.php}{reference}).}
        \end{figure}
        \item Numpy array defines the array-centric data structure
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Numpy array basics}
    \begin{minipage}{0.5\linewidth}
        \begin{itemize}
            \item Similar to list (number only)
            \vspace{5mm}
            \item Optimized for numerical processing
            \vspace{5mm}
            \item Default: row-major (different to Matlab)
            \vspace{5mm}
            \item Search Matlab function's Numpy equivalent is a good start point
        \end{itemize}
    \end{minipage}
    \hspace{2mm}
    \begin{minipage}{0.45\linewidth}
        \begin{figure}
            \centering
            \includegraphics[width=\linewidth]{nparray_basics}
        \end{figure}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Array creation}
    \begin{itemize}
        \item 1-D array
        \begin{figure}
            \centering
            \includegraphics[width=\linewidth]{array1}
            \caption{Create 1-D array of integer and float numbers.}
        \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Array creation (cont')}
    \begin{itemize}
        \item 2-D array
        \begin{figure}
            \centering
            \includegraphics[width=\linewidth]{array2}
            \caption{Create 2-D array of float numbers.}
        \end{figure}
        \item Complex array
        \begin{figure}
            \centering
            \includegraphics[width=\linewidth]{array3}
            \caption{Create 1-D array of complex numbers.}
        \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Initiate a Numpy array}
    \begin{itemize}
        \item zeros
        \item ones
        \item empty
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{initiate_nparray1}
        \caption{Examples of initiate Numpy array}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Initiate a large array (cont')}
    \begin{itemize}
        \item arange
        \begin{figure}
            \centering
            \includegraphics[width=\linewidth]{initiate_nparray2}
            \caption{Examples of initiate Numpy array}
        \end{figure}
        \item linspace
        \begin{figure}
            \centering
            \includegraphics[width=\linewidth]{initiate_nparray2}
            \caption{Examples of initiate Numpy array}
        \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Basic operations}
    \begin{minipage}{0.5\linewidth}
        \begin{itemize}
            \item Element-wise summation (+) and multiplication (*)
            \vspace{10mm}
            \item Matrix multiplication (@ or np.dot() function)
            \vspace{10mm}
            \item Comparison results in a boolean Numpy array
        \end{itemize}
    \end{minipage}
    \begin{minipage}{0.48\linewidth}
        \begin{figure}
            \centering
            \includegraphics[width=\linewidth]{operation}
            \caption{Basic operation for Numpy array.}
        \end{figure}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Indexing and slicing}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{slice-1d}
        \caption{Index and slice 1-D Numpy array.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Indexing and slicing (cont')}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\linewidth]{slice-2d}
        \caption{Index and slice 2-D Numpy array.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Shape manipulation}
    \begin{figure}
        \centering
        \includegraphics[width=0.75\linewidth]{shape1}\\
        \includegraphics[width=0.75\linewidth]{shape2}
        \caption{Examples to manipulate Numpy array shapes.}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Stack Numpy array}
%    \begin{figure}
%        \begin{minipage}{0.4\linewidth}
%            \centering
%            \includegraphics[width=1.05\linewidth]{stack1}
%        \end{minipage}
%        \begin{minipage}{0.58\linewidth}
%            \centering
%            \includegraphics[width=1.2\linewidth]{stack2}
%        \end{minipage}
%        \caption{Examples of stacking Numpy arrays.}
%    \end{figure}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Split Numpy array}
%    \begin{figure}
%        \centering
%        \includegraphics[width=\linewidth]{split}
%        \caption{Split large Numpy array into smaller ones.}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Copy Numpy Array}
    \begin{itemize}
        \item No copy at all
        \vspace{-5mm}
        \begin{figure}
            \centering
            \includegraphics[width=1.2\linewidth]{copy1}
        \end{figure}
        \item Shallow copy
        \vspace{-5mm}
        \begin{figure}
            \centering
            \includegraphics[width=1.2\linewidth]{copy2}
        \end{figure}
        \item Deep copy
        \vspace{-5mm}
        \begin{figure}
            \centering
            \includegraphics[width=1.2\linewidth]{copy3}
        \end{figure}
    \end{itemize}
\end{frame}

\section{Working Examples}
% Objectives:
%   1. A fully working example
%   2. DIY:
%   3. scikit-learn implementation


\begin{frame}
	\frametitle{Logistic regression and gradient descent}
	Packages we need:
	{\scriptsize \inputminted{python}{code/lr_package.py}}
\end{frame}

\begin{frame}
	\frametitle{Logistic regression and gradient descent}
	Plot the data
	{\scriptsize \inputminted{python}{code/lr_plot_data.py}}
	\begin{figure}
		\centering
		\includegraphics[width=0.5\linewidth]{lr_data}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Logistic regression and gradient descent}
	\begin{itemize}
		\item logistic function 
		\[h(x) = \frac{1}{1 + e^{-x}}.\]
		\item cost function (negative log-likelihood)
		\[
			f(\beta) = -\sum_{i=1}^{100}y_{i}log(h(x_{i})) + (1-y_{i})log(1-h(x_{i})).
		\]
		where $x_i = \beta^T w_i$
		\item derivative of cost function with respect to $\beta$
		\[  
			\partial f /\partial \beta  = -\sum_{i=1}^{100} (y_i - h(\beta^T w_i))w_i.
		\]
		\item gradient step
		\[
			\beta^{t+1} = \beta^{t} - \alpha  \partial f /\partial \beta. 
		\]
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Logistic regression and gradient descent}
	Now Let's first convert the math preliminaries into code.
	{\scriptsize \inputminted{python}{code/lr_math.py}}
\end{frame}

\begin{frame}
	\frametitle{Logistic regression and gradient descent}
	Probably the most challenging...write the GD algorithm.
	{\scriptsize \inputminted{python}{code/lr_gd.py}}
\end{frame}

\begin{frame}
	\frametitle{Logistic regression and gradient descent}
	Check if the algorithm converges
	\begin{figure}
		\centering
		\includegraphics[width=0.6\linewidth]{lr_gradient}
	\end{figure}
	However we can directly use LR from sklearn.
	{\scriptsize \inputminted{python}{code/lr_sklearn.py}}
\end{frame}

\begin{frame}
	\frametitle{Logistic regression and gradient descent}
	\begin{itemize}
		\item Gradient descent is one way to learn the coefficients
		\item More advanced algorithm: BFGS, L-BFGS, conjugate gradient
		\item Good news is that they are provided in scipy and can be easily run in Python once you have defined your cost function and your gradients.
	\end{itemize}
	{\scriptsize \inputminted{python}{code/lr_l-bfgs.py}}	
	See the complete source code for this example in \emph{lrgd.py}.
\end{frame}

\begin{frame}
	\frametitle{LDA vs. QDA}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\linewidth]{LDAQDA}
		\caption{LDA vs. QDA}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{LDA vs. QDA}
% \lstinputlisting[language=Python]{code/plot_lda_qda.py}
 {\scriptsize \inputminted{python}{code/lda_package.py}}
\end{frame}

\begin{frame}
	\frametitle{LDA vs. QDA}
	% \lstinputlisting[language=Python]{code/plot_lda_qda.py}
	\begin{itemize}
		\item Generate random data
		\item Set random seed so that results are repeatable
	\end{itemize}
	{\scriptsize \inputminted{python}{code/lda_generate_data.py}}
\end{frame}

\begin{frame}
	\frametitle{LDA vs. QDA}
	\begin{itemize}
		\item plot data
	\end{itemize}
	{\tiny \inputminted{python}{code/lda_plot_data.py}}
\end{frame}

\begin{frame}
	\frametitle{LDA vs. QDA}
	\begin{itemize}
		\item plot ellipse
	\end{itemize}
	{\scriptsize \inputminted{python}{code/lda_eclipse.py}}
\end{frame}

\begin{frame}
	\frametitle{LDA vs. QDA}
	\begin{itemize}
		\item put things together:
	\end{itemize}
	{\scriptsize \inputminted{python}{code/lda_main.py}}
	See the complete source code for this example in \em{plot\_lda\_qda.py}.
\end{frame}


\section{Summary}
% Objectives:
%   1. What you can go from here?
%   2. Resources
%   3. Take away message

\begin{frame}
    \frametitle{Take home messages}
    \begin{itemize}
        \item Python is a well-round programming language for scientific computing
        \item Abundant community support and available toolboxes
        \item Full support for vector algebra, optimization, machine learning
        \item Aesthetic and efficient visualization tools
        \item Fully functioning package manager and IDE
        \item Unified experience on three major OS's
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Useful resources}
    \begin{itemize}
        \item Python language reference: \url{https://docs.python.org/3/reference/index.html}
        \item Anaconda: \url{https://anaconda.org/}
        \item PyCharm: \url{https://www.jetbrains.com/pycharm/}
        \item SciPy.org: \url{http://scipy.org/}
        \item Seaborn: \url{http://seaborn.pydata.org/}
        \item scikit-image: \url{http://scikit-image.org/}
        \item scikit-learn: \url{http://scikit-learn.org/stable/}
        \item TensorFlow: \url{https://www.tensorflow.org/}
        \item GitHub: \url{https://github.com/}
        \item StackOverflow: \url{http://stackoverflow.com/}
        \item PyPI: \url{https://pypi.python.org/pypi}
    \end{itemize}
\end{frame}

\end{document}
