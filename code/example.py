import numpy as np


def foo(x):
    """ Compute square-root of x """
    if x < 0:
        raise ValueError("Expect only non-negative value.")
    else:
        return np.sqrt(x)


if __name__ == '__main__':
    print(foo(2.0))
    print(foo(-2.0))
