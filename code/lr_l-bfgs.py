from scipy.optimize import fmin_l_bfgs_b
# normalize data
norm_X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)
myargs = (norm_X, y_flip)
betas = np.zeros(norm_X.shape[1])
lbfgs_fitted = fmin_l_bfgs_b(cost_func, x0=betas, args=myargs, fprime=log_gradient)
print(lbfgs_fitted[0])
