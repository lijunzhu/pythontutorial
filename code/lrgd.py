"""
====================================================================
Example : Logistic Regression Using Gradient Descent
====================================================================

Logistic Regression Using Gradient Descent
"""

# __future__ is a pseudo-module which programmers can use to enable new language features which are not
# compatible with the current interpreter. Usually you don't need it.
# with future statement:
#   print 8/7  # prints 1.1428571428571428
#   print 8//7 # prints 1
# without future statement:
#   print 8/7  # prints 1
#   print 8//7 # prints 1
from __future__ import division
# import some existing datasets from sklearn
from sklearn import datasets
# matplotlib contains basic plot functions.
import matplotlib.pyplot as plt
# seaborn is a statisticl data visualization toolbox
import seaborn as sns
# setting axes style to 'ticks' and color palette to 'set2'
sns.set(style='ticks', palette='Set2')
# panda is a data analysis toolkit suitable for relational or labelled data.
import pandas as pd
# numpy: fast array/matrix manipulation and computing.
import numpy as np
# math: contains mathematic functions
import math

# load the iris dataset
data = datasets.load_iris()
# keep the first 2 attributes, and the first 100 data
X = data.data[:100, :2]
# keep the corresponding labels
y = data.target[:100]

# plot in figure 1
fig = plt.figure(num=1)
# scatter plot of data points from the two classes respectively
setosa = plt.scatter(X[:50,0], X[:50,1], c='b')
versicolor = plt.scatter(X[50:,0], X[50:,1], c='r')

# add labels for x and y axes
plt.xlabel("Sepal Length")
plt.ylabel("Sepal Width")
plt.legend((setosa, versicolor), ("Setosa", "Versicolor"))
# removing spines
sns.despine()


# implement logistic function
def logistic_func(theta, x):
    return float(1) / (1 + math.e**(-x.dot(theta)))

# calculate the gradient of cost function
def log_gradient(theta, x, y):
    first_calc = logistic_func(theta, x) - np.squeeze(y)
    final_calc = first_calc.T.dot(x)
    return final_calc

# cost function, which is negative log-likelihood in logistic regression
def cost_func(theta, x, y):
    log_func_v = logistic_func(theta,x)
    y = np.squeeze(y)
    step1 = y * np.log(log_func_v)
    step2 = (1-y) * np.log(1 - log_func_v)
    final = -step1 - step2
    return np.mean(final)

# implement the gradient descent method (constant learning rate)
def grad_desc(theta_values, X, y, lr=.001, converge_change=.001):
    # normalize
    X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)
    # setup cost iter
    cost_iter = []
    cost = cost_func(theta_values, X, y)
    cost_iter.append([0, cost])
    change_cost = 1
    i = 1
    while (change_cost > converge_change):
        old_cost = cost
        theta_values = theta_values - (lr * log_gradient(theta_values, X, y))
        cost = cost_func(theta_values, X, y)
        cost_iter.append([i, cost])
        change_cost = old_cost - cost
        i += 1
    return theta_values, np.array(cost_iter)

# Predict test data using learned theta. Return the likelihood of belonging to class 1 or labels when hard == True.
def pred_values(theta, X, hard=True):
    # normalize
    X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)
    pred_prob = logistic_func(theta, X)
    pred_value = np.where(pred_prob >= .5, 1, 0)
    if hard:
        return pred_value
    return pred_prob


shape = X.shape[1]
y_flip = np.logical_not(y)  # flip Setosa to be 1 and Versicolor to zero to be consistent
betas = np.zeros(shape)
fitted_values, cost_iter = grad_desc(betas, X, y_flip)
print(fitted_values)

# classify the 100 data points.
predicted_y = pred_values(fitted_values, X)
# number of correct classifications
print(np.sum(y_flip == predicted_y))

# plot cost per iteration to check whether the algorithm converges.
fig = plt.figure(num=2)
plt.plot(cost_iter[:, 0], cost_iter[:, 1])
plt.ylabel("Cost")
plt.xlabel("Iteration")
sns.despine()

# show our plots.
plt.show()

# Use implemented logistic regression from sklearn
from sklearn import linear_model
logreg = linear_model.LogisticRegression()
logreg.fit(X, y_flip)
print(sum(y_flip == logreg.predict(X)))


